<?php
/**
 * Template Name: Home
 */

get_header(); ?>

  <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>
      <?php 
        $hero_section = get_field( 'hero_section' );
        $title = $hero_section['main_title'];
        $text = $hero_section['supporting_text'];
      ?>
      <section id="hero" class="section hero">
        <div class="grid-container hero__container">
          <div class="grid-x align-middle">
            <div class="hero__box cell" data-aos="fade">
              <div class="grid-container hero__content hero__top">
                <div class="grid-x grid-margin-x">
                  <div class="cell">
                    <!-- <img src="<?= get_template_directory_uri() . '/assets/images/sciops-logo-white.png'; ?>" alt="" /> -->
                    <h1 data-aos="fade-up" data-aos-delay="300" ><?php echo $title; ?></h1>
                  </div> <!-- .cell --> 
                </div> <!-- .grid-x -->
              </div> <!-- .hero__content -->
            
              <div class="grid-container hero__content hero__bottom">
                <div class="grid-x grid-margin-x">
                  <div class="cell">
                    <p class="hero-tag" data-aos="fade-up" data-aos-delay="300"><?php echo $text; ?></p>
                  </div> <!-- .hero__box -->
                </div> <!-- .grid-x --> 
              </div> <!-- .grid-container -->
            </div> <!-- .hero__box -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->

        <div class="hero-frame1"></div>
        <div class="hero-frame2"></div>
        <div class="hero-frame3"></div>
        <div class="hero-frame4"></div>
        <div class="hero-frame5"></div>

      </section>

      <section id="hero-mobile" class="section m_hero">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/homepage-img-cluster-mobile.png" alt="">
        <!-- <div class="grid-container full m_hero_container">
          <div class="grid-x">
            <div class="cell">
              <div class="m_hero-frame1"></div>
              <div class="m_hero-frame2"></div>
              <div class="m_hero-frame3"></div>
              <div class="m_hero-frame4"></div>
              <div class="m_hero-frame5"></div>
            </div> 
          </div> 
        </div>  -->
      </section>

    <?php endwhile; ?>

    <section id="featured-survey" class="section">
      <div class="grid-container">
        <div class="grid-x align-center">
          <div class="cell">
            <h2 class="lg-header text-center" data-aos="fade-up" data-aos-delay="300">Featured Surveys</h2>
          </div>
        </div>
      </div>
      <div class="grid-container full">
        <div class="grid-x align-center">
          <div class="featured-surveys cell">
            <?php 
              $i=0;  
              $survey_query = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 8
              ));
              while( $survey_query->have_posts() ) : $survey_query->the_post(); $i++; ?>
                <div class="survey-slide">
                  <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
                    <div class="box flex-container flex-dir-column align-center survey-item fade-in">
                      <h2><?php the_title(); ?></h2>
                      <p><?php the_excerpt(); ?></p>
                      <p class="date"><?php the_time('F Y'); ?></p>
                      <span class="open"></span>
                      <?php if( has_post_thumbnail() ) : ?>
                        <div class="box__image" style="background-image: url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large'); ?>);"></div>
                      <?php endif; ?>
                    </div> <!-- .box --> 
                  </a> 
                </div>
            <?php endwhile; 
            wp_reset_postdata(); ?>
          </div>
          
          
        </div> <!-- .grid-x --> 
      </div> <!-- .grid-container -->
      <div class="grid-container">
        <div class="grid-x align-center">
          <div class="cell">       
            <p class="text-center"><a href="/survey-results" class="view-all">View All Surveys</a></p>
          </div>
        </div>
      </div>
    </section>
    <?php 
        $m_section = get_field( 'white_section' );
        $m_title = $m_section['section_title'];
        $m_text = $m_section['section_copy'];
      ?>
    <section id="members-panel" class="section">
      <!-- <div class="parallax-bg"></div> -->
      <div class="grid-container">
        <div class="grid-x grid-margin-x align-middle">
          <div class="cell medium-10 large-6">
            <h2 class="lg-multi-header" data-aos="fade-up" data-aos-delay="300"><?php echo $m_title; ?></h2>
            <p data-aos="fade-up" data-aos-delay="300"><?php echo $m_text; ?></p>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
      <img class="p-image-mobile" src="<?php echo get_template_directory_uri(); ?>/assets/images/parallax-imgs-mobile.png" alt="">
      <div class="p-image"></div>
    </section>

    <section id="what-we-do" class="section">
      <div class="grid-container">
        <div class="grid-x grid-padding-y align-center text-center">
        <?php while( have_rows('what_we_do') ) : the_row(); 
          $c_name = get_sub_field( 'column_name');
          $c_header = get_sub_field( 'column_header' );
          $c_excerpt = get_sub_field( 'column_excerpt');
        ?>
          <div class="cell large-4 flex-container flex-dir-column align-middle <?php echo $c_name; ?>">
            <div class="bodymovin-wrapper animated fadeIn">
              <div id="<?php echo $c_name; ?>"></div>
            </div> <!-- .bodymovin -->
            <!-- <img src="/wp-content/themes/sciops/assets/images/collect.png" width="225" height="225" alt="ASU Sciops Collect Data" /> -->
            
            <h2 class="lg-header" data-aos="fade-up" data-aos-delay="200"><?php echo $c_header; ?></h2>
            <p data-aos="fade-up" data-aos-delay="300"><?php echo $c_excerpt; ?></p>
          </div> <!-- .cell -->
        <?php endwhile; ?>
        <div id="anchor"></div>
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="divider" class="section m_Divider">
      <div class="grid-container full">
        <div class="grid-x">
          <div class="cell large-12">
            <hr style="background:#79818A; height:1px">
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>
    
    <?php 
        $w_section = get_field( 'who_we_are' );
        $w_title = $w_section['section_title'];
        $w_text = $w_section['section_copy'];
      ?>
    <section id="who-we-are" class="section">
      <div class="grid-container full">
        <div class="grid-x align-center text-center">
          <div class="cell large-12">
            <h2 class="lg-header" data-aos="fade-up" data-aos-delay="300"><?php echo $w_title; ?></h2>
            <h3 data-aos="fade-up" data-aos-delay="300"><?php echo $w_text; ?></h3>
            <div data-aos="fade-up" data-aos-delay="300"><?php wp_nav_menu(array('theme_location' => 'menu-access')); ?></div>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

<?php get_footer(); ?>
