<?php

/**
 * Template Name: Survey Results
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>

    <section id="survey-results" class="section">
      <div class="grid-container">
        <div class="grid-x grid-margin-x align-center">
          
              <?php 
              $i=0;  
              $survey_query = new WP_Query(array(
                'post_type' => 'post'
              ));
              while( $survey_query->have_posts() ) : $survey_query->the_post();
              $i++;
              
              if( $i%4 == 0 || $i%4 == 1) {
                $box_size = 'box__square';
              } elseif( $i%4 == 2 || $i%4 == 3) {
                $box_size = 'box__rect';
              }
              ?>
              <div class="cell medium-6 survey-item">
                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
                  <div class="box box__left box-<?php echo $i%4; ?> <?php echo $box_size; ?> <?php if( $i%4 == 3 ) { echo 'box__offset'; } ?> flex-container flex-dir-column align-center survey-item fade-in" data-aos="fade-up" data-aos-delay="300">
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_excerpt(); ?></p>
                    <p class="date"><?php the_time('F Y'); ?></p>
                    <span class="open"></span>
                    <?php if( has_post_thumbnail() ) : ?>
                      <div class="box__image" style="background-image: url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large'); ?>);"></div>
                    <?php endif; ?>
                  </div> <!-- .box --> 
                </a>
              </div>
              <?php endwhile; wp_reset_postdata(); ?>
              
            
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="about-surveys" class="section">
      <div class="grid-container">
        <div class="grid-x grid-padding-x grid-margin-x">
          <div class="cell small-12">
            <h3 class="lg-multi-header" data-aos="fade-up" data-aos-delay="300"><?php the_field( 'headline' ); ?></h3>
      
          </div> <!-- .cell -->
          <div class="cell large-7" data-aos="fade-up" data-aos-delay="300">
            <?php the_field( 'copy' ); ?>
            <?php while( have_rows( 'topics_section' ) ) : the_row(); ?>
              <h5 class="text-white" data-aos="fade-in" data-aos-delay="300"><?php the_sub_field( 'topics_title' ); ?></h5>

              <div class="grid-container grid-x full" data-aos="fade-up" data-aos-delay="300">
                <div class="cell large-12">
                    <ul class="topics-list">
                      <?php while( have_rows( 'list_of_topics') ) : the_row(); ?>
                        <li><?php the_sub_field( 'topic' ); ?></li>
                      <?php endwhile; ?>
                  </ul>
                </div>
              </div>
            <?php endwhile; ?>
          </div><!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>
  
<?php else : ?>

<?php get_template_part( 'partials/content', 'none' ); ?>

<?php endif; ?>

<?php get_footer(); ?>
