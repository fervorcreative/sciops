<?php

/**
 * Template Name: Our Process
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>

  <?php while ( have_posts() ) : the_post(); ?>
    <?php 
        $hero_section = get_field( 'hero_section' );
        $title = $hero_section['main_title'];
      ?>
    <section id="our-process" class="section m_Headline header">
      <div class="grid-container">
        <div class="grid-x align-middle">
          <div class="cell large-10">
            <h1 class="lg-multi-header text-white" data-aos="fade-up" data-aos-delay="300"><?php echo $title; ?></h1>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>
    
    
    <?php 
    $i=0;
    while( have_rows('what_we_do') ) : the_row(); 
      $i++;
      $c_name = get_sub_field( 'column_name');
      $c_header = get_sub_field( 'column_header' );
      $c_excerpt = get_sub_field( 'column_excerpt'); ?>
      <section id="<?php echo $c_name; ?>" class="section m_ImageCopy">
        <div class="grid-container">
          <div class="grid-x">
            
            <div class="cell large-5 large-offset-1 small-order-2 large-order-1 flex-container flex-dir-column align-center">
              <h2 class="lg-header" data-aos="fade-up" data-aos-delay="300"><?php echo $c_header; ?></h2>
              <p data-aos="fade-up" data-aos-delay="300"><?php echo $c_excerpt; ?></p>
            </div> <!-- .cell --> 

            <div class="cell large-6 small-order-1 large-order-2 flex-container flex-dir-column align-middle">
              <div class="bodymovin-wrapper-svg-svg">
                <div id="we-<?php echo $c_name; ?>"></div>
              </div> <!-- .bodymovin -->
            </div> <!-- .cell -->
            <div id="anchor<?php echo $i; ?>"></div>
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->
      </section>
    <?php endwhile; ?>
    

    <section id="divider" class="section m_Divider white-bg">
      <div class="grid-container full">
        <div class="grid-x">
          <div class="cell large-12">
            <hr/>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="transparency" class="section m_Headline white-bg">
      <div class="grid-container">
        <div class="grid-x align-center align-middle text-center">
          <div class="cell large-8 transparency">
            <h2 class="text-blue lg-multi-header" data-aos="fade-up" data-aos-delay="300"><?php the_field( 'mission_statement' ); ?></h2>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="divider" class="section divider white-bg" style="">
      <div class="grid-container full">
        <div class="grid-x">
          <div class="cell large-12">
            <hr/>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>
    <?php while( have_rows( 'method_section' ) ) : the_row(); ?>
    <section id="our-method" class="section">
      <div class="grid-container">
        <div class="grid-x grid-margin-x align-center">
          <div class="cell large-12">
            <h2 class="lg-header" data-aos="fade-up" data-aos-delay="300"><?php the_sub_field( 'section_title'); ?></h2>
          </div>
          <?php 
          
          $count = 0;
          $columns = get_sub_field( 'copy');
          $num_col = count( $columns );
          $col_class = 'small-12';
          //echo $num_col;
          if( $num_col == 2 ) {
            $col_class = 'small-12 large-6';
          } elseif( $num_col == 3 ) {
            $col_class = 'small-12 large-4';
          }
          while( have_rows( 'copy' ) ) : the_row(); ?>
            <div class="cell <?php echo $col_class; ?>" data-aos="fade-up" data-aos-delay="300">
              <?php the_sub_field( 'column_text' );  ?>
            </div> <!-- .cell -->
          <?php endwhile; ?>

          
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>
    <?php endwhile; ?>

  <?php endwhile; ?>
  
<?php else : ?>

<?php get_template_part( 'partials/content', 'none' ); ?>

<?php endif; ?>

<?php get_footer(); ?>
