  </main>

  <footer class="footer color-white" role="contentinfo">
    <div class="grid-container footer__container">
      <div class="grid-x grid-margin-x align-top align-center">
        <div class="cell small-12 small-order-2 medium-order-1 medium-6 large-6 footer__copyright__container">
          <img src="<?= get_stylesheet_directory_uri() . '/assets/images/sciops-logo.svg'; ?>" alt="" />
          <h4 class="footer__copyright">SciOPS (Scientist Opinion Panel Survey) is a science communication platform developed by Arizona State University’s Center for Science, Technology and Environmental Policy Studies. Learn more at <a href="https://csteps.asu.edu" target="_blank">csteps.asu.edu</a></h4>
          <img src="<?= get_stylesheet_directory_uri() . '/assets/images/asu-sciops-logo@2x.png'; ?>" alt="" width="283" />
          <h4 class="footer__copyright">© 2020 Arizona State University. All rights reserved.</h4>
        </div> <!-- .cell --> 

        <div class="cell small-12 small-order-1 medium-order-2 medium-4 medium-offset-2 large-3 large-offset-3 footer__links">
          <?php wp_nav_menu(array('location' => 'menu-footer')); ?>
        </div> <!-- .footer__icons -->
      </div> <!-- .grid-x --> 
    </div> <!-- .grid-container -->
  </footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
