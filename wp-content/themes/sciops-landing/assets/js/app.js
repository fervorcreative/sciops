/* global AOS */

// Set up App object and jQuery
var App = App || {},
  $ = $ || jQuery;

App.navLink = function() {
  $('.js-nav-toggle').on( 'click', function() {
    // Show nav overlay
    if ($('html').hasClass('is-open')) {
      $('.nav--header').fadeToggle(200);
      $('html').toggleClass('is-open');
    // Hide nav overlay
    } else {
      $('.nav--header').fadeToggle(250);
      window.setTimeout(function() {
        $('html').toggleClass('is-open');
      }, 50);
    }
  });
};

App.fadeAnimations = function() {
  AOS.init({
    disable: 'tablet',
    offset: 200,
    duration: 1000,
    once: true,
  });
}

App.smoothScroll = function() {
  $('a[href^=\\#]:not([href=\\#])').on('click', function(event) {
    var target = $.attr(this, 'href');
    var targetPosition = $(target).offset().top;
    var currentPosition = $('.site').offset().top;

    $('html, body').stop().animate({
        scrollTop: targetPosition - currentPosition
    }, 400);

    event.preventDefault();
  });
}

App.sliderInit = function() {
  $('.center').slick({
    centerMode: true,
    arrows: true,
    centerPadding: '25%',
    slidesToShow: 1,
    adaptiveHeight: false,
    variableWidth: false,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
  });
}

App.promoBarToggle = function() {
  var timeInHours = .25
  var expirationDuration = 1000 * 60 * 60 * timeInHours;
  var prevAccepted = localStorage.getItem('promoShown');
  var currentTime = new Date().getTime();
  var notAccepted = prevAccepted === undefined;
  var prevAcceptedExpired = prevAccepted !== undefined && currentTime - prevAccepted > expirationDuration
  if (notAccepted || prevAcceptedExpired) {
    localStorage.setItem('promoShown', currentTime);
  } else {
    $('#promo-bar').hide();
  }
} //function

App.promoClose = function() {
  $('.promo__close').on('click', function() {
    $('#promo, .promo__close').slideUp(250, function() {
      $('header').css('top', '0');
    });
    $('#logo').removeClass('hidden');
  });
}

App.triggerVirtualTour = function() {
  $('.virtual-tour-link a').addClass('fancybox-iframe');
}

// Instantiate functions when document is ready
$(document).ready(function() {
  // App.navLink();
  App.smoothScroll();
  App.sliderInit();
  App.navLink();
  App.triggerVirtualTour();
});

// Instantiate functions when document has loaded most assets such as images
$(window).on('load', function() {
  App.fadeAnimations();
});
