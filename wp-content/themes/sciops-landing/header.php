<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://use.typekit.net/ysw6usq.css">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
  </head>

  <body <?php if ( get_body_class() ) body_class(); ?> data-aos="fade">

    <?php do_action('after_body'); ?>

    <div class="site">
      <main id="main" role="main">
