  </main>

  <footer class="footer color-white" role="contentinfo">
    <div class="grid-container full footer__container">
      <div class="grid-x align-top align-center">
        <div class="cell small-12 medium-12 large-12 copyright">
          <div class="grid-container full">
            <div class="grid-x">

              <div class="cell small-12 medium-8 large-8 footer__copyright__container">
                <h4 class="footer__copyright">SciOPS (Scientist Opinion Panel Survey) is a science communication platform developed by Arizona State University’s<br/> Center for Science, Technology and Environmental Policy Studies. Learn more at <a href="https://csteps.asu.edu" target="_blank">csteps.asu.edu</a></h4>
              </div> <!-- .cell --> 

              <div class="cell small-12 medium-4 large-4 footer__links">
                <?php wp_nav_menu(array('location' => 'menu-footer')); ?>
              </div> <!-- .footer__icons -->

            </div> <!-- .grid-x -->
          </div> <!-- .grid-container -->
        </div> <!-- .cell --> 
      </div> <!-- .grid-x --> 
    </div> <!-- .grid-container -->
  </footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
