<?php
/**
 * The front page template
 */

get_header(); ?>

  <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

      <section id="hero" class="hero">
        <div class="grid-container full hero__container">
          <div class="grid-x align-middle">
            <div class="hero__box cell" data-aos="fade">
              <div class="grid-container full hero__content hero__top">
                <div class="grid-x">
                  <div class="cell">
                    <img src="<?= get_template_directory_uri() . '/assets/images/sciops-logo-white.png'; ?>" alt="" />
                    <h1>We connect people<br/> to science opinion</h1>
                  </div> <!-- .cell --> 
                </div> <!-- .grid-x -->
              </div> <!-- .hero__content -->
            
              <div class="grid-container full hero__content hero__bottom">
                <div class="grid-x">
                  <div class="cell">
                    <h2>Access, visualize and share <br/>expert opinion from a national <br/>panel of US scientists</h2>
                    
                    <h3><a href="mailto:lforst@asu.edu">Contact Us</a></h3>
                  </div> <!-- .hero__box -->
                </div> <!-- .grid-x --> 
              </div> <!-- .grid-container -->
            </div> <!-- .hero__box -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->

        <div class="hero-frame1"></div>
        <div class="hero-frame2"></div>
        <div class="hero-frame3"></div>
        <div class="hero-frame4"></div>
        <div class="hero-frame5"></div>

      </section>

      <section class="m_hero">
        <div class="grid-container full m_hero_container">
          <div class="grid-x">
            <div class="cell">
              <div class="m_hero-frame1"></div>
              <div class="m_hero-frame2"></div>
              <div class="m_hero-frame3"></div>
              <div class="m_hero-frame4"></div>
              <div class="m_hero-frame5"></div>
            </div> <!-- .cell -->
          </div> <!-- .grid-x -->
        </div> <!-- .hero_container -->
      </section>

    <?php endwhile; ?>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

<?php get_footer(); ?>
