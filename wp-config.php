<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y7Lhj1M2t7v+KjG5vXjI0x40GTgYSQrsfWT03dunupoCcDuoW+gv0KjQpaaqg6RA+MrIkP/eFuo394N52GNoyw==');
define('SECURE_AUTH_KEY',  'h7hujoojH6XDYgvHt1B+xByCSqRNwpw7jDVFdUwa+Y6E6JuJRmOLRwuJCn4xpRuro6bAdOHRNqonU9qtGVNbWg==');
define('LOGGED_IN_KEY',    'MhY32rOLrYUqaZZC0jnpMd1uFO7M5WtE+555kZPS0dtjai69bMypYirjPJg103dhN0f6e0mo+BdGjqEAvqeMIg==');
define('NONCE_KEY',        '1rc/ot5t4V03XMZE+/l4MXNa6nd2eYHmBPYu5rgGpW+nyGnvchX0J9LJSl7+5OWhlHXXf+ai/w1RAwk7HzyCfQ==');
define('AUTH_SALT',        'Aa6O2X7PboyOpvyhh2HelG+6/CwnYzumGSiz1pOJSD5kqDXWBJRTT1d18IhQemX208uO1FgjWRDU5cIZMJeOTA==');
define('SECURE_AUTH_SALT', 'iv5uK8YDbTEnYtSt5VXN90ImUY2HyjlQe6GTML4mL2rKN1Aw0QYQE0Qpj+3M/kShN5Da8B9z0y2pkw9cvAUSAw==');
define('LOGGED_IN_SALT',   'BrtbaB4brzZtzluGdrzeITgU+2Ysre4dIw/nG9Bw5w9JBeQHIg3zJiQAjrNC4nlDLpqmgKMLnNI5oLV89/qFSg==');
define('NONCE_SALT',       '1f0iOjTbdXVQ3vXU5KYjXyqwgPQXwRVVTCYL1vj5HCNN7BiNiBHZ8xpLVyRhYw1VQvbtbSLGQVymRzPkPnqBIg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
