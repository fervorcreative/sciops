<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://use.typekit.net/ysw6usq.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
  </head>

  <body <?php if ( get_body_class() ) body_class(); ?>>

    <header id="header" class="section">
      <div class="grid-container">
        <div class="grid-x align-middle">
          <div class="cell large-3 logo-container">
            <a href="<?= home_url(); ?>">
              <img src="<?= get_stylesheet_directory_uri() . '/assets/images/sciops-logo.svg'; ?>" alt="asu-sciops-logo" />
            </a>
          </div> <!-- .cell -->
          
          <div class="cell large-9 menu-container">
            <?php wp_nav_menu(array('theme_location' => 'menu-header')); ?>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->

      <div class="nav-link js-nav-toggle">
        <div class="nav-link__outer">
          <div class="nav-link__icon"></div>
        </div> <!-- .nav-link -->
      </div> <!-- .nav-link -->
    </header>

    <nav class="header__nav">
      <div class="container">
        <?php 
          wp_nav_menu (
            array(
              'theme_location' => 'menu-header-offcanvas',
              'container' => '',
              'items_wrap' => '<nav class="%2$s">%3$s</nav>',
              'menu_class' => 'nav nav--header'
            )
          ); ?>
      </div>
    </nav>

    <div class="site">
      <main id="main" role="main">
