<?php

/**
 * Template Name: Our Team
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>

  <?php while ( have_posts() ) : the_post(); ?>

    <section id="people" class="section m_PageHeader meet-people">
      <div class="grid-container">
        <div class="grid-x">
          <div class="cell large-12">
            <h1 class="lg-multi-header" data-aos="fade-up" data-aos-delay="300"><?php the_field( 'main_title' ); ?></h1>
          </div>
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="stats" class="section">
      <div class="grid-container">
        <div class="grid-x align-center align-top text-center">
          <?php 
          $i=4; while( have_rows('stats') ) : the_row(); $i++; $stat_delay = $i * 200; ?>
            <div class="cell large-4 stat-box" data-aos="fade-up" data-aos-delay="<?php echo $stat_delay; ?>">
              <span class="stat text-white"><?php the_sub_field('stat_number'); ?></span>
              <span class="stat-title text-white flex-container flex-dir-column"><?php the_sub_field('stat_title'); ?></span>
              <span class="stat-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="31" height="31" viewBox="0 0 31 31">
                  <g transform="translate(930 -462) rotate(90)">
                    <line y2="30" transform="translate(477.5 899.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="1"/>
                    <line x2="30" transform="translate(462.5 914.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="1"/>
                  </g>
                </svg>
              </span>
              <!-- <span class="stat-toggle text-white">+</span> -->
              
              <div class="stat-copy">
                <?php the_sub_field('stat_copy'); ?>
              </div> <!-- .stat-copy -->

            </div>
          <?php endwhile; ?>
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    
    <?php while( have_rows( 'team_profiles' ) ) : the_row(); ?>
    <section id="team" class="section">
     <div class="grid-container">
        <div class="grid-x grid-margin-x grid-padding-y">
          <div class="cell">
            <h2 class="lg-header" data-aos="fade-right" data-aos-delay="300"><?php the_sub_field('section_title'); ?></h2>
          </div>
        </div>
      </div>
      <?php while( have_rows( 'team' ) ) : the_row(); 
        $dept = get_sub_field( 'team_department' ); 
        $dept_slug = str_replace(' ', '-', $dept );
        $dept_slug = strtolower( $dept_slug );
      ?>
        <div id="<?php echo $dept_slug; ?>" class="grid-container">
          <div class="grid-x grid-margin-x grid-padding-y">
            <?php if( get_sub_field( 'team_department' ) ) : ?>
              <div class="cell"><h3><?php the_sub_field('team_department'); ?></h3></div>
            <?php endif; ?>
            
            <?php 
            $j=0; 
            while( have_rows( 'team_profiles' ) ) : the_row(); $j++; $aos_delay = $j * 150; ?>
              <div class="cell medium-6 large-3" data-aos="fade-up" data-aos-offset="0" data-aos-delay="<?php echo $aos_delay; ?>">
                <?php 
                $img = get_sub_field( 'portrait' );
                echo wp_get_attachment_image( $img['ID'], 'medium' ); ?>
                <span class="member-name"><?php the_sub_field( 'name' ); ?></span>
                <span class="member-title"><?php the_sub_field( 'title' ); ?></span>
              </div> <!-- .cell -->
            <?php endwhile; ?>
            
          </div>
        </div>
        
      <?php endwhile; ?>
    </section>
    <?php endwhile; ?>

    <section id="team" class="section" style="display:none">
      <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y">

          <h2>Our Advisory Board</h2>
          
          <!-- Advisory 1-4 --> 

          <div class="cell medium-6 large-3">
            <img src="<?= get_stylesheet_directory_uri() . '/assets/images/advisory1.jpg'; ?>"" alt="" />
            <span class="member-name">First Name Last Name</span>
            <span class="member-title">Advisory Board Title</span>
          </div> <!-- .cell -->

          <div class="cell medium-6 large-3">
            <img src="<?= get_stylesheet_directory_uri() . '/assets/images/advisory2.jpg'; ?>"" alt="" />
            <span class="member-name">First Name Last Name</span>
            <span class="member-title">Advisory Board Title</span>
          </div> <!-- .cell -->

          <div class="cell medium-6 large-3">
            <img src="<?= get_stylesheet_directory_uri() . '/assets/images/advisory3.jpg'; ?>"" alt="" />
            <span class="member-name">First Name Last Name</span>
            <span class="member-title">Advisory Board Title</span>
          </div> <!-- .cell -->

          <div class="cell medium-6 large-3">
            <img src="<?= get_stylesheet_directory_uri() . '/assets/images/advisory4.jpg'; ?>"" alt="" />
            <span class="member-name">First Name Last Name</span>
            <span class="member-title">Advisory Board Title</span>
          </div> <!-- .cell -->

          <!-- Advisory 5-8 --> 

          <div class="cell medium-6 large-3">
            <img src="<?= get_stylesheet_directory_uri() . '/assets/images/advisory5.jpg'; ?>"" alt="" />
            <span class="member-name">First Name Last Name</span>
            <span class="member-title">Advisory Board Title</span>
          </div> <!-- .cell -->

          <div class="cell medium-6 large-3">
            <img src="<?= get_stylesheet_directory_uri() . '/assets/images/advisory6.jpg'; ?>"" alt="" />
            <span class="member-name">First Name Last Name</span>
            <span class="member-title">Advisory Board Title</span>
          </div> <!-- .cell -->

          <div class="cell medium-6 large-3">
            <img src="<?= get_stylesheet_directory_uri() . '/assets/images/advisory7.jpg'; ?>"" alt="" />
            <span class="member-name">First Name Last Name</span>
            <span class="member-title">Advisory Board Title</span>
          </div> <!-- .cell -->

          <div class="cell medium-6 large-3">
            <img src="<?= get_stylesheet_directory_uri() . '/assets/images/advisory8.jpg'; ?>"" alt="" />
            <span class="member-name">First Name Last Name</span>
            <span class="member-title">Advisory Board Title</span>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>
    
    <?php
    if( have_rows('call_to_action' ) ) : 
      while( have_rows('call_to_action' ) ) : the_row(); ?> 
        <section id="work-together" class="section">
          <div class="grid-container">
            <div class="grid-x grid-margin-x">
              <div class="cell large-12">
                <h2 class="text-white" data-aos="fade-up" data-aos-delay="300"><?php the_sub_field( 'headline' ); ?></h2>
              </div> <!-- .cell -->
            </div> <!-- .grid-x --> 

            <div class="grid-x grid-margin-x" data-aos="fade-up" data-aos-delay="400">
              <?php 
              $count = 0;
              $columns = get_sub_field( 'copy');
              $num_col = count( $columns );
              $col_class = 'small-12';
              //echo $num_col;
              if( $num_col == 2 ) {
                $col_class = 'small-12 large-6';
              } elseif( $num_col == 3 ) {
                $col_class = 'small-12 large-4';
              }
              while( have_rows('copy') ) : the_row(); ?>
                <div class="cell <?php echo  $col_class; ?>">
                  <?php the_sub_field( 'text_column' ); ?>
                </div> <!-- .cell --> 
              <?php endwhile; ?>
            </div> <!-- .grid-x -->
          </div> <!-- .grid-container -->
        </section>
    <?php endwhile; 
    endif; ?>

  <?php endwhile; ?>
  
<?php else : ?>

<?php get_template_part( 'partials/content', 'none' ); ?>

<?php endif; ?>

<?php get_footer(); ?>
