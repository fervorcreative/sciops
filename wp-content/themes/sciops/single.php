<?php

get_header(); 

$study_group1 = get_field('general_overview'); 
$study_group2 = get_field('response_overview');
$study_group3 = get_field('survey_demographics');
$questions = get_field('questions'); 

?>

<section id="regulation" class="section m_PageHeader">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="cell large-12">
        <h1 class="lg-multi-header"><?php the_title(); ?></h1>
      </div>
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="survey-data" class="section">
  <div class="grid-container">
    <div class="grid-x grid-margin-x align-center">
      <div class="cell large-4">
        <strong>General Overview</strong>

        <div class="data-box general-overview">
          <div class="data-table">
            <div class="data-row">
              <div class="data-label">Study Date:</div>
              <div class="data-value"><?= $study_group1['study_date']; ?></div>
            </div> <!-- .data-row --> 

            <div class="data-row">
              <div class="data-label">Geographic Coverage:</div>
              <div class="data-value"><?= $study_group1['geographic_coverage']; ?></div>
            </div> <!-- .data-row --> 

            <div class="data-row">
              <div class="data-label">Expertise:</div>
              <div class="data-value data-value-space"><?= $study_group1['expertise']; ?></div>
            </div> <!-- .data-row -->
          </div> <!-- .data-table -->
        </div> <!-- .data-box" -->
      </div> <!-- .cell -->

      <div class="cell large-4">
        <strong>Response Overview</strong>

        <div class="data-box response-overview">
          <div class="data-table">
            <div class="data-row">
              <div class="data-label">Sample Size:</div>
              <div class="data-value"><?= $study_group2['sample_size']; ?></div>
            </div> <!-- .data-row --> 

            <div class="data-row">
              <div class="data-label">Valid Responses:</div>
              <div class="data-value"><?= $study_group2['valid_responses']; ?></div>
            </div> <!-- .data-row --> 

            
            <div class="data-row">
              <div class="data-label">Response rate:</div>
              <div class="data-value"><?= $study_group2['response_rate']; ?></div>
            </div> <!-- .data-row -->

            <div class="data-row">
              <div class="data-label">Date initial findings posted:</div>
              <div class="data-value"><?= $study_group2['date_initial_findings']; ?></div>
            </div> <!-- .data-row -->

            <div class="data-row">
              <div class="data-label">Most recent update:</div>
              <div class="data-value"><?= $study_group2['most_recent_update']; ?></div>
            </div> <!-- .data-row -->

            <div class="data-row">
              <div class="data-label">Days survey in field:</div>
              <div class="data-value"><?= $study_group2['days_in_field']; ?></div>
            </div> <!-- .data-row -->

            <div class="data-row">
              <div class="data-label">Average response time:</div>
              <div class="data-value"><?= $study_group2['average_response_time']; ?></div>
            </div> <!-- .data-row -->
          </div> <!-- .data-table -->
        </div> <!-- .data-box" -->

        <div class="data-box survey-demographics">
          <div class="data-table">
            
          </div> <!-- .data-table -->
        </div> <!-- .data-box" -->
      </div> <!-- .cell -->

      <div class="cell large-4">
        <strong>Survey Demographics</strong>

        <div class="data-box">
          <div class="data-table">
            <div class="data-row">
              <div class="data-label respondent-demographics">Respondent Demographics:</div>
              <div class="data-value"><?= $study_group3['respondent_demographics']; ?></div>
            </div> <!-- .data-row --> 

            <div class="data-row">
              <div class="data-label">Language(s):</div>
              <div class="data-value"><?= $study_group3['languages']; ?></div>
            </div> <!-- .data-row --> 
            
            
          </div> <!-- .data-table -->
          <?php 
          $ft_bool = $study_group3['add_footnote'];
          $ftnoote = $study_group3['footnote']; 
          if( $ft_bool && !empty( $ftnoote ) ) { ?>
            <div class="data-footnote">
              <p><?php echo $ftnoote; ?></p>
            </div>
          <?php } ?>
          
        </div> <!-- .data-box" -->

        <div class="data-box">
          <div class="data-table">

          </div> <!-- .data-table -->
        </div> <!-- .data-box" -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<?php  if( have_rows('pre_survey_description') ) : ?>
    <?php while( have_rows('pre_survey_description') ) : the_row(); ?>
      
        <?php 
        
        $count = 0;
        $columns = get_sub_field( 'copy');
        if( $columns ) {
          $num_col = count( $columns );
        }
        
        $col_class = 'small-12';
        if( have_rows('copy') ) : ?>
        
          <section id="pre-survey-description" class="section survey-description pre-survey">
            <div class="grid-container <?php echo ( $num_col > 1 ) ? 'narrow' : 'slim'; ?>">
              <div class="grid-x grid-margin-x">
                <?php $pre_s_title = get_field('pre_survey_title');
                if( $pre_s_title ) : ?>
                  <div class="cell small-12">
                    <h5 class="text-uppercase"><?php echo $pre_s_title; ?></h5>
                  </div>
                <?php endif; ?>
                <?php
                  
                  
                  
                  if( $num_col == 2 ) {
                    $col_class = 'small-12 large-6';
                  } elseif( $num_col == 3 ) {
                    $col_class = 'small-12 large-4';
                  }
                  while( have_rows('copy') ) : the_row(); ?>
                    <div class="cell <?php echo $col_class; ?>">
                      <?php the_sub_field( 'text' ); ?>
                    </div>
                  <?php endwhile; ?>
                
              </div>
            </div>
          </section>
        <?php endif; ?>
            
    <?php endwhile; ?>
  <?php endif; ?>


<?php 
  if ($questions): 
    foreach ($questions as $question): ?>

      <section class="survey-graphs" class="section" data-aos="fade-up" data-aos-delay="300">
        <div class="grid-container" style="background-color: <?= $question['background_color']; ?>">
          <div class="grid-x grid-padding-x align-center text-center">
            <div class="cell small-12 large-12">
              <div class="questions-container">
              <?php if( !empty( $question['question'] ) ) : ?>
                <?php 
                  $hide_q_title = $question['question_title']; // false to show
                  if( !$hide_q_title ) echo '<h5>Question</h5>'; ?>
                <div class="question"><?= $question['question']; ?></div>
              <?php endif; ?>
                <?php echo wp_get_attachment_image($question['image']['ID'], 'large'); ?>
                <?php
                $hide_f_title = $question['findings_title']; // false to show
                if( !$hide_f_title ) echo '<h5 class="finding">Finding</h5>';
                ?>
                <div class="copy"><?= $question['finding']; ?></div>
              </div> <!-- .questions-container -->
            </div> <!-- .cell --> 
          </div>
        </div> <!-- .grid-container -->
      </section>
      <?php
    endforeach;
  endif; ?>
  
  <?php  if( have_rows('survey_footnote') ) : ?>
    <?php while( have_rows('survey_footnote') ) : the_row(); ?>
      
      <?php 
      $main_headline = get_sub_field( 'main_headline' );
      $count = 0;
      $columns = get_sub_field( 'main_copy');
      if( $columns ) {
        $num_col = count( $columns );
      }
      $col_class = 'small-12';
      if( have_rows( 'main_copy' ) ) : ?>
        <section id="survey-footnote" class="section footnote unset" data-aos="fade-up" data-aos-delay="300">
          <div class="grid-container slim">
            <div class="grid-x grid-margin-x align-center">
              <div class="cell small-12">
                <h5 class="text-center"><?php echo $main_headline; ?></h5>
              </div>
              <?php
              
              if( $num_col == 2 ) {
                $col_class = 'small-12 large-6';
              } elseif( $num_col == 3 ) {
                $col_class = 'small-12 large-4';
              }
              while( have_rows('main_copy') ) : the_row(); ?>
                <div class="cell <?php echo $col_class; ?>" style="max-width: 812px;">
                  <?php the_sub_field( 'column_text' ); ?>
                </div>
              <?php endwhile; ?>
            </div>
          </div>
        </section>
      <?php endif; ?>
          
    <?php endwhile; ?>
  <?php endif; ?>
  
  <?php  if( have_rows('survey_description') ) : ?>
    <?php while( have_rows('survey_description') ) : the_row(); ?>
      
        <?php 
        
        $count = 0;
        $columns = get_sub_field( 'copy');
        if( $columns ) {
          $num_col = count( $columns );
        }
        
        $col_class = 'small-12';
        if( have_rows('copy') ) : ?>
        
          <section id="survey-description" class="section footnote survey-description">
            <div class="grid-container slim">
              <div class="grid-x grid-margin-x">
                <div class="cell small-12">
                  <h5 class="text-uppercase">Survey Description</h5>
                </div>
                <?php
                  
                  
                  
                  if( $num_col == 2 ) {
                    $col_class = 'small-12 large-6';
                  } elseif( $num_col == 3 ) {
                    $col_class = 'small-12 large-4';
                  }
                  while( have_rows('copy') ) : the_row(); ?>
                    <div class="cell <?php echo $col_class; ?>">
                      <?php the_sub_field( 'text' ); ?>
                    </div>
                  <?php endwhile; ?>
                
              </div>
            </div>
          </section>
        <?php endif; ?>
            
    <?php endwhile; ?>
  <?php endif; ?>
  
  
  
  <?php $args=array(
    'post__not_in' => array($post->ID),
    'posts_per_page'=> 2,
  );

  $related_posts = new WP_Query($args); ?>

<section id="related-surveys" class="section bg-white">
  <div class="grid-container">
    <div class="grid-x grid-padding-x align-center">
      <div class="cell small-12 large-10">
        <h5 class="text-blue" data-aos="fade-up" data-aos-delay="300">Additional Surveys</h5>
        <div class="grid-container full grid-x">
          <?php if($related_posts->have_posts()): while ($related_posts->have_posts()): $related_posts->the_post(); ?>
            <div class="cell large-6">
              <h3 data-aos="fade-up" data-aos-delay="300"><a style="text-decoration:none; color:#2a3746" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            </div> <!-- .cell --> 
          <?php endwhile; endif; wp_reset_postdata(); ?>
        </div> <!-- .grid-container -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<?php get_footer(); ?>
