/* global AOS, bodymovin, revapi1, tpj, gsap, ScrollTrigger */

// Set up App object and jQuery
var App = App || {},
  $ = $ || jQuery;

  App.navToggle = function() {
    $(document).on('click', '.js-nav-toggle', function() {
      if ( $('html').hasClass('is-open') ) {
        $('.nav-contain').fadeToggle(200);
        $('.header').css({'background-color' : '#fff'});
        $('.button--nav').css('visibility', 'visible');
        $('html').toggleClass('is-open');
        $('.logo-container img').attr('src', '/wp-content/themes/sciops/assets/images/sciops-logo.svg');
      } else {
        $('#promo-bar').hide();
        $('.nav-contain').fadeToggle(250);
        $('.header').css({'background-color' : '#000'});
        $('.button--nav').css('visibility', 'hidden');
        $('.logo-container img').attr('src', '/wp-content/themes/sciops/assets/images/sciops-logo-blue.svg');
        window.setTimeout( function() {
          $('html').toggleClass('is-open');
        }, 50 );
      }
    });
  } //function

App.fadeAnimations = function() {
  AOS.init({
    disable: 'mobile',
    offset: 200,
    duration: 800,
    once: true,
  });
}

App.smoothScroll = function() {
  $('a[href^=\\#]:not([href=\\#])').on('click', function(event) {
    var target = $.attr(this, 'href');
    var targetPosition = $(target).offset().top;
    var currentPosition = $('.site').offset().top;

    $('html, body').stop().animate({
        scrollTop: targetPosition - currentPosition
    }, 400);

    event.preventDefault();
  });
}

App.statToggle = function () {
  $('.stat-box .stat-copy').hide();
  $('.stat-toggle').on('click', function(e) {
    if ($(this).parent().hasClass('active')) {
      // hide the stuff
      //$(this).parent().parent().removeClass('align-middle').addClass('align-top');
      $(this).toggleClass('stat-toggle-rotated');
      $(this).parent().removeClass('active');
      $(this).next('.stat-copy').slideUp(400);
    } else {
      // show the stuff
      //$(this).parent().parent().removeClass('align-top').addClass('align-middle');
      $(this).toggleClass('stat-toggle-rotated');
      $(this).parent().addClass('active');
      $(this).next('.stat-copy').slideDown(400);
    }
  });
}

App.createLogoAnimationHome = function() {

  function isInViewHome(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height() - 200;
    var elemTop = $(elem).offset().top;
    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
  }

  $(window).on('scroll.animations1 touchmove.animations1', function() {

    if (isInViewHome($('#anchor'))) {

      setTimeout(function() {

        var animCollect;
        var animAnalyze;
        var animShare;

        var elemCollect = document.getElementById('collect');
        var elemAnalyze = document.getElementById('analyze');
        var elemShare = document.getElementById('share');

        var collectData = {
          container: elemCollect,
          renderer: 'svg',
          loop: false,
          autoplay: false,
          rendererSettings: {
              progressiveLoad:true
          },
          path: '/wp-content/themes/sciops/animations/collect.json'
        };

        var analyzeData = {
          container: elemAnalyze,
          renderer: 'svg',
          loop: false,
          autoplay: false,
          rendererSettings: {
              progressiveLoad:true
          },
          path: '/wp-content/themes/sciops/animations/analyze.json'
        };

        var shareData = {
          container: elemShare,
          renderer: 'svg',
          loop: false,
          autoplay: false,
          rendererSettings: {
              progressiveLoad:true
          },
          path: '/wp-content/themes/sciops/animations/share.json'
        };

        animCollect = bodymovin.loadAnimation(collectData);
        animAnalyze = bodymovin.loadAnimation(analyzeData);
        animShare = bodymovin.loadAnimation(shareData);

        setTimeout(function(){ animCollect.play(); }, 100);
        setTimeout(function(){ animAnalyze.play(); }, 700);
        setTimeout(function(){ animShare.play(); }, 1400);
          
      }, 100);

      $(window).off('scroll.animations1 touchmove.animations1');

    } //endif
  }); //(window).scroll
}
App.createLogoAnimationProcess = function() {

  function isInViewProcess(elem) {
    var docViewTop2 = $(window).scrollTop();
    var docViewBottom2 = docViewTop2 + $(window).height() - 200;
    var elemTop2 = $(elem).offset().top;
    return ((elemTop2 <= docViewBottom2) && (elemTop2 >= docViewTop2));
  }

  $(window).on('scroll.animations2 touchmove.animations2', function() {

    if (isInViewProcess($('#anchor1'))) {
      setTimeout(function () {
        var elemWeCollect = document.getElementById('we-collect');
        var animWeCollect;
        var weCollectData = {
          container: elemWeCollect,
          renderer: 'svg',
          loop: false,
          autoplay: true,
          rendererSettings: {
            progressiveLoad: true
          },
          path: '/wp-content/themes/sciops/animations/we-collect.json'
        }

        animWeCollect = bodymovin.loadAnimation(weCollectData);
        setTimeout(function () { animWeCollect.play(); }, 100);
      }, 100);
      $(window).off('scroll.animations2 touchmove.animations2');
    }
    if (isInViewProcess($('#anchor2'))) {
      setTimeout(function () {
        var elemWeAnalyze = document.getElementById('we-analyze');
        var animWeAnalyze;
        var weAnalyzeData = {
          container: elemWeAnalyze,
          renderer: 'svg',
          loop: false,
          autoplay: true,
          rendererSettings: {
            progressiveLoad: true
          },
          path: '/wp-content/themes/sciops/animations/we-analyze.json'
        }

        animWeAnalyze = bodymovin.loadAnimation(weAnalyzeData);
        setTimeout(function () { animWeAnalyze.play(); }, 100);
      }, 100);
      $(window).off('scroll.animations2 touchmove.animations2');
    }
    if (isInViewProcess($('#anchor3'))) {
      setTimeout(function() {
        var elemWeShare = document.getElementById('we-share');
        var animWeShare;
        var weShareData = {
          container: elemWeShare,
          renderer: 'svg',
          loop: false,
          autoplay: true,
          rendererSettings: {
            progressiveLoad:true
          },
          path: '/wp-content/themes/sciops/animations/we-share.json'
        }

        animWeShare = bodymovin.loadAnimation(weShareData);
        setTimeout(function(){ animWeShare.play(); }, 100);

      }, 100);

      $(window).off('scroll.animations2 touchmove.animations2');
     
    }
  });
}

App.processIconAnimations = function () {
  var elemWeCollect = document.getElementById('we-collect');
  var elemWeAnalyze = document.getElementById('we-analyze');
  var elemWeShare = document.getElementById('we-share');
  var setActions = 'play none reset none';
  ScrollTrigger.create({
    trigger: '#anchor1',
    once: true,
    scrub: false,
    //toggleActions: setActions,
    //markers: true,
    onEnter: animateAnchor1
    //onUpdate: ({progress, direction, isActive}) => console.log(progress, direction, isActive)
  });
  ScrollTrigger.create({
    trigger: '#anchor2',
    once: true,
    scrub: false,
    //toggleActions: setActions,
    //markers: true,
    onEnter: animateAnchor2
    //onUpdate: ({progress, direction, isActive}) => console.log(progress, direction, isActive)
  });
  ScrollTrigger.create({
    trigger: '#anchor3',
    once: true,
    scrub: false,
    //toggleActions: setActions,
    //markers: true,
    onEnter: animateAnchor3
    //onUpdate: ({progress, direction, isActive}) => console.log(progress, direction, isActive)
  });
  function animateAnchor1() {
    //console.log('play anchor1');
    var animateWeCollect = bodymovin.loadAnimation({
      container: elemWeCollect,
      renderer: 'svg',
      loop: false,
      autoplay: true,
      rendererSettings: {
        progressiveLoad: true
      },
      path: '/wp-content/themes/sciops/animations/we-collect.json'
    });
  }
  function animateAnchor2() {
    //console.log('play anchor1');
    var animateWeAnalyze = bodymovin.loadAnimation({
      container: elemWeAnalyze,
      renderer: 'svg',
      loop: false,
      autoplay: true,
      rendererSettings: {
        progressiveLoad: true
      },
      path: '/wp-content/themes/sciops/animations/we-analyze.json'
    });
  }
  function animateAnchor3() {
    //console.log('play anchor1');
    var animateWeShare = bodymovin.loadAnimation({
      container: elemWeShare,
      renderer: 'svg',
      loop: false,
      autoplay: true,
      rendererSettings: {
        progressiveLoad: true
      },
      path: '/wp-content/themes/sciops/animations/we-share.json'
    });
  }
}

App.surveyCarousel = function () {
  
  var $surveys = $('.featured-surveys').flickity({
    pageDots: false,
    wrapAround: true,
    contain: true,
    groupCells: 1,
    percentPosition: false
  });
  var flickity = $surveys.data('flickity');
  
  
  
  // $surveys.on('staticClick.flickity', function(event, pointer, cellElement, cellIndex) {
  //   if (!cellElement) {
  //     return;
  //   }
  //   $surveys.flickity('select', cellIndex);    
  // });
  
  
}
App.customLink = function() {
  
  $('.slidelink').on('click', function(e) {

    var is_active = $(this).closest('[data-isactiveslide]');
    var slide_link = $(this).closest('[data-link]');
    var slide_index = $(this).closest('[data-originalindex]');
    
    if (is_active !== 'true') {
      revapi1 = tpj('#rev_slider_1_1');
      revapi1.revshowslide(slide_index.data('originalindex'));
    }

  });
};

App.parallaxBGs = function () {
  //console.log('animate');
  gsap.registerPlugin(ScrollTrigger);
  gsap.to('.p-image', {
    scrollTrigger: {
      trigger: '#members-panel',
      //toggleActions: 'restart none none none',
      scrub: true
    },
    yPercent: 20
  });
  
  
}

// Instantiate functions when document is ready
$(document).ready(function() {
  App.smoothScroll();
  App.statToggle();
  App.navToggle();

  if ($('body').hasClass('home')) {
    App.createLogoAnimationHome();
    App.surveyCarousel();
    App.parallaxBGs();
    
    
  }

  if ($('body').hasClass('our-process')) {
    //App.createLogoAnimationProcess();
    App.processIconAnimations();
  }

});

// Instantiate functions when document has loaded most assets such as images
$(window).on('load', function() {
  App.fadeAnimations();
  //App.customLink();
});
