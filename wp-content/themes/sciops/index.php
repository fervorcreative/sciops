<?php
/**
 * The main template file
 */

get_header(); ?>

<main role="main">

  <?php if ( have_posts() ) : ?>

    <section class="section">
      <div class="grid-container">
        <div class="grid-x small-up-2">

          <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'partials/content', get_post_format() ); ?>

          <?php endwhile; ?>

        </div>
      </div>
    </section>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

</main>

<?php get_footer(); ?>
